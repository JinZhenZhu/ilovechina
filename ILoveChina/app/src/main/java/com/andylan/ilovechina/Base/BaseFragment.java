package com.andylan.ilovechina.Base;

import android.support.v4.app.Fragment;
import android.view.View;

public abstract class BaseFragment  extends Fragment implements View.OnClickListener{

    public BaseActivity _context;

    public void showProgress(){

        _context.showProgress();
    }

    public void CloseProgress(){

        _context.closeProgress();
    }

    public void showToast(String strMsg){

        _context.showToast(strMsg);
    }

    public void showAlert(String strMsg){

        _context.showAlertDialog(strMsg);
    }

    @Override
    public void onClick(View v) {

    }
}
