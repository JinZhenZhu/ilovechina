package com.andylan.ilovechina.Adapter;

import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.BaseAdapter;
import android.widget.ImageView;

import com.andylan.ilovechina.Main.CultureListActivity;
import com.andylan.ilovechina.Model.CultureModel;
import com.andylan.ilovechina.R;
import com.squareup.picasso.Picasso;

import java.util.ArrayList;

/**
 * Created by STS on 12/2/2017.
 */

public class CultureListAdapter extends BaseAdapter {

    CultureListActivity activity;

    ArrayList<CultureModel> allData =  new ArrayList<>();

    public CultureListAdapter(CultureListActivity activity) {
        this.activity = activity;
    }

    public void refresh(ArrayList<CultureModel> data){
        allData.clear();
        allData.addAll(data);
        notifyDataSetChanged();
    }

    @Override
    public int getCount() {
        return allData.size();
    }

    @Override
    public CultureModel getItem(int position) {
        return allData.get(position);
    }

    @Override
    public long getItemId(int position) {
        return position;
    }

    @Override
    public View getView(int position, View convertView, ViewGroup parent) {

        CustomHolder holder;

        if (convertView ==  null){
            holder = new CustomHolder();
            LayoutInflater inflater = LayoutInflater.from(activity);
            convertView =  inflater.inflate(R.layout.item_culture, parent, false);
            holder.setId(convertView);
            convertView.setTag(holder);

        }else {
            holder = (CustomHolder)convertView.getTag();
        }

        CultureModel model = allData.get(position);
        holder.setData(model);

        return convertView;
    }

    private class CustomHolder{

        ImageView imvPicture;

        private void setId(View view){
            imvPicture =  (ImageView)view.findViewById(R.id.imvPicture);
        }

        private void setData(CultureModel model){
            imvPicture.setImageResource(model.getResourceId());
        }
    }
}
