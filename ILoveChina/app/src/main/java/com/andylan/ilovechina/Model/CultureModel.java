package com.andylan.ilovechina.Model;

import java.io.Serializable;

/**
 * Created by STS on 12/2/2017.
 */

public class CultureModel implements Serializable{

    int id = 0, resourceId = 0;
    String webUrl;

    public int getId() {
        return id;
    }

    public void setId(int id) {
        this.id = id;
    }

    public int getResourceId() {
        return resourceId;
    }

    public void setResourceId(int resourceId) {
        this.resourceId = resourceId;
    }

    public String getWebUrl() {
        return webUrl;
    }

    public void setWebUrl(String webUrl) {
        this.webUrl = webUrl;
    }
}
