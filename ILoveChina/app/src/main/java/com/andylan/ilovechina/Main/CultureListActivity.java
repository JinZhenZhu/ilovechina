package com.andylan.ilovechina.Main;

import android.content.Intent;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.view.View;
import android.widget.AdapterView;
import android.widget.ListView;

import com.andylan.ilovechina.Adapter.CultureListAdapter;
import com.andylan.ilovechina.Base.BaseActivity;
import com.andylan.ilovechina.CustomWebView.SonicJavaScriptInterface;
import com.andylan.ilovechina.Model.CultureModel;
import com.andylan.ilovechina.R;

import java.util.ArrayList;

public class CultureListActivity extends BaseActivity {

    CultureListAdapter mCultureListAdapter;

    ListView lstCultureList;

    public static final int MODE_SONIC_WITH_OFFLINE_CACHE = 2;
    public static final int MODE_DEFAULT = 0;
    public static final int MODE_SONIC = 1;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_culture_list);

        loadLayout();
    }

    private void loadLayout() {

        mCultureListAdapter = new CultureListAdapter(this);
        lstCultureList = (ListView)findViewById(R.id.lstCultureList);
        lstCultureList.setAdapter(mCultureListAdapter);

        lstCultureList.setOnItemClickListener(new AdapterView.OnItemClickListener() {
            @Override
            public void onItemClick(AdapterView<?> adapterView, View view, int i, long l) {

                String URL = mCultureListAdapter.getItem(i).getWebUrl();
                Intent intent = new Intent(CultureListActivity.this, BrowserActivity.class);
                intent.putExtra(BrowserActivity.PARAM_URL, URL);
                intent.putExtra(BrowserActivity.PARAM_MODE, MODE_SONIC);
                intent.putExtra(SonicJavaScriptInterface.PARAM_CLICK_TIME, System.currentTimeMillis());
                startActivityForResult(intent, -1);
            }
        });

        loadData();
    }

    private void loadData() {

        Integer[] resourceList = {R.drawable.virtues, R.drawable.gongfu, R.drawable.tea, R.drawable.food, R.drawable.panda, R.drawable.festival, R.drawable.opera};
        String[] urlList = {"https://rampages.us/garstai/2016/02/09/traditional-chinese-virtues/", "http://www.visitourchina.com/guide/culture/chinese-kung-fu.html",
                "http://www.visitourchina.com/guide/culture/chinese-tea.html", "http://www.visitourchina.com/guide/culture/chinese-cuisine.html",
                "http://www.visitourchina.com/guide/culture/chinese-pandas.html", "http://www.visitourchina.com/guide/culture/chinese-festival.html",
                "http://www.visitourchina.com/guide/culture/china-opera.html"};

        ArrayList<CultureModel> allData = new ArrayList<>();

        for (int i = 0; i < resourceList.length; i++){

            CultureModel data =  new CultureModel();
            data.setResourceId(resourceList[i]);
            data.setWebUrl(urlList[i]);

            allData.add(data);
        }

        mCultureListAdapter.refresh(allData);
    }
}
